// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // agencyBaseUrl: 'http://167.172.100.241:4321/api/v1/',
  agencyBaseUrl: '',
  baseUrl: 'http://localhost:4111/api/v1/',
  // baseUrl: 'http://localhost:5111/api/v1/',
};

// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
