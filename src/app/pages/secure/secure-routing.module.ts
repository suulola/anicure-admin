import { BroadcastComponent } from './broadcast/broadcast.component';
import { ThirdPartiesComponent } from './report/third-parties/third-parties.component';
import { LogComponent } from './report/log/log.component';
import { ViewusersComponent } from './admin/viewusers/viewusers.component';
import { AgentPurseComponent } from './agency/agent-purse/agent-purse.component';
import { CentralPurseComponent } from './agency/central-purse/central-purse.component';
import { AgencySettingsComponent } from './agency/agency-settings/agency-settings.component';
import { RoleGuard } from './../../guard/role.guard';
import { AuthenticationGuard } from './../../guard/authentication.guard';
import { LoanSettingsComponent } from './loan/loan-settings/loan-settings.component';
import { LoanManagementComponent } from './loan/loan-management/loan-management.component';
import { ReferralComponent } from './report/referral/referral.component';
import { FeedbackComponent } from './report/feedback/feedback.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { UsersComponent } from './users/users.component';
import { AuditComponent } from './admin/audit/audit.component';
import { LoanDashboardComponent } from './loan/loan-dashboard/loan-dashboard.component';
import { PermissionsComponent } from './admin/permissions/permissions.component';
import { SecureComponent } from './secure.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BranchesComponent } from './report/branches/branches.component';

const routes: Routes = [{
  path: '',
  component: SecureComponent,
  children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
      path: "dashboard",
      component: DashboardComponent,
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'doctors',
      component: TransactionsComponent,
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'broadcast',
      component: BroadcastComponent,
      canActivate: [AuthenticationGuard],
    },
    {
      path: 'users',
      children: [
        {
          path: '',
          pathMatch: 'full',
          component: UsersComponent,
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'details/:id',
          component: UserDetailComponent,
          canActivate: [AuthenticationGuard],
        },

      ]
    },
    {
      path: 'report',
      children: [
        {
          path: 'referral',
          component: ReferralComponent,
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'feedback',
          component: FeedbackComponent,
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'log',
          component: LogComponent,
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'platforms',
          component: ThirdPartiesComponent,
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'branches',
          component: BranchesComponent,
          canActivate: [AuthenticationGuard],
        },
      ]
    },
    {
      path: 'setting',
      children: [
        {
          path: 'audit', component: AuditComponent, 
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'password', 
          component: ResetpasswordComponent, 
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'permission', 
          component: PermissionsComponent, 
          canActivate: [AuthenticationGuard],
        },
        {
          path: 'admin-user', 
          component: ViewusersComponent, 
          canActivate: [AuthenticationGuard],
        },
      ]
    },
    {
      path: 'loans', children:
        [
          {
            path: 'management',
            component: LoanManagementComponent,
            canActivate: [AuthenticationGuard],
          },
          {
            path: 'dashboard', 
            component: LoanDashboardComponent, 
            canActivate: [AuthenticationGuard],
          },
          {
            path: 'settings', 
            component: LoanSettingsComponent, 
            canActivate: [AuthenticationGuard],
          },
          

        ]
    },
  ]
}
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecureRoutingModule { }
// export const AuthRoutes = RouterModule.forChild(routes);
