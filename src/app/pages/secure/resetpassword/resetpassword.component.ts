import { AuthService } from '../../../services/auth/auth.service';
import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

 
  hide = true;
  cHide = true;
  loading: boolean = false;
  resetForm: FormGroup; 

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
  ) { }

  resetPassword(): void {
    this.loading = true;
    this.router.navigate(['/auth/login']);
  }

  goBack() {
    this.router.navigate(['/auth/login']);
  }

  ngOnInit(): void {
    this.resetForm = this.formBuilder.group({
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(4)])),
      cpassword: new FormControl('', Validators.compose([Validators.required, Validators.minLength(4)]))
    });    
   }

   
}
