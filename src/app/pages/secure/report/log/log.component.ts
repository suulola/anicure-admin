import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { exportConfig,  transactionStatus } from 'src/app/constants/constant';
import { ReportService } from 'src/app/services/report/report.service';
import { ISelection } from 'src/app/models/iselection';
import { UtilsService } from 'src/app/services/utils/utils.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit, AfterViewInit {

 
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['position', 'user', 'message', 'dbAssociated', 'data', 'date'];
  dataSource: any = new MatTableDataSource([]);;
  exportAsConfig: ExportAsConfig = exportConfig('pdf', 'referral_table', 'Referral')
  maxDate: Date;
  isLoadingResults = true;
  requestModel: any = {}
  allStatus: ISelection[] = transactionStatus;
  maxall: number = 1000;

  constructor(
    private reportService: ReportService,
    private utilService: UtilsService,
    private exportAsService: ExportAsService,
  ) { }

  ngOnInit() {
    const currentYear = new Date();
    this.maxDate = new Date(currentYear);
    
    this.fetchLog(10, 1,)
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  fetchLog(limit: number, page: number) {
    this.reportService.getAllNotificationLog(limit, page).subscribe(async (response: any) => {
      console.log(response.data.docs)
      console.log(response)
      this.dataSource = new MatTableDataSource(response.data.docs);

      this.maxall = response.data.meta.total;
      this.isLoadingResults = false
    }, (error: any) => {
      this.isLoadingResults = false
      this.utilService.triggerNotification(error.status ? 'Error fetching data' : 'Network Issues. Try again')
    })
  }

  

  onPageFired(event) {
    this.isLoadingResults = true;
    this.fetchLog(event.pageSize, event.pageIndex + 1)
  }

  exportTable(type: any) {
    this.exportAsConfig.type = type;
    this.exportAsService.save(this.exportAsConfig, 'Log Report').subscribe((response) => {
      this.utilService.triggerNotification('File Downloaded')
    }, (error) => {
      console.log(error)
      this.utilService.triggerNotification(error)
    });
  }


}
