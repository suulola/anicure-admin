import { differentAccountTypes, exportConfig } from './../../../constants/constant';
import { UtilsService } from './../../../services/utils/utils.service';
import { Router } from '@angular/router';
import { UserService } from './../../../services/user/user.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['position', 'fullName', 'mobileNumber', 'status', 'otp', 'farmName', 'farmAddress', 'localGovernment', 'state', 'callSubscription', 'chatSubscription', 'farmService', 'date'];
  dataSource: any = new MatTableDataSource([]);;
  dataSourceUnSorted: any;
  userFilterForm: FormGroup;
  exportAsConfig: ExportAsConfig = exportConfig('pdf', 'userlist_table', 'Users')

  maxDate: Date;
  value = '';
  isLoadingResults = false;
  userRequestModel: any = {};

  maxall: number = 1000;

  constructor(
    private userService: UserService,
    public formBuilder: FormBuilder,
    private exportAsService: ExportAsService,
    private router: Router,
    private utilService: UtilsService
  ) { }

  ngOnInit() {
    const currentYear = new Date();
    this.maxDate = new Date(currentYear);

    this.getAllUserTransactions(10, 1)
  }
  

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  handleSuccessResponse = async (response) => {
    console.log(response, '*****')
    this.dataSourceUnSorted = await new MatTableDataSource(response.data.docs);
    this.dataSource = await this.dataSourceUnSorted;
    this.maxall = response.data.meta.total;
    this.isLoadingResults = false;
  }

  handleFailureResponse = async (error) => {
    this.utilService.triggerNotification(error.status ? 'Error fetching data' : 'Network Issues. Try again')     
    this.isLoadingResults = false;
    this.dataSource = new MatTableDataSource([]);
  }

  getAllUserTransactions(limit: number, page: number) {
    this.isLoadingResults = true;
    this.userService.getAllUsers(limit, page).subscribe(async (response: any) => {
      this.handleSuccessResponse(response)
    }, (error: any) => {
      this.handleFailureResponse(error);
    })
  }

  async search() {
    let { startDate, endDate, mobileNumber, customerName: fullName, accountType: source } = this.userFilterForm.value;
    startDate = startDate ? new Date(startDate).toDateString() : ''
    endDate = endDate ? new Date(endDate).toDateString() : ''

    this.userRequestModel.startDate = startDate;
    this.userRequestModel.endDate = endDate;
    this.userRequestModel.mobileNumber = mobileNumber;
    this.userRequestModel.source = source;
    this.userRequestModel.fullName = fullName;

    await this.getAllUserTransactions(10, 1)
  }

  onPageFired(event) {
    this.getAllUserTransactions(event.pageSize, event.pageIndex + 1)
  }

  async routeToDetails(user) {
    const stringifiedUser = JSON.stringify(user)
    this.router.navigate(['app/transactions-details', { state: stringifiedUser }])
  }

  exportTable(type: any) {
    this.exportAsConfig.type = type;
    this.exportAsService.save(this.exportAsConfig, 'Accion Transactions').subscribe((response) => {
      // save started
    }, (err) => {
      this.utilService.triggerNotification(err)     
      console.log(err)
    });
  }
}
