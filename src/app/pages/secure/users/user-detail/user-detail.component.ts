import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { LoanService } from 'src/app/services/loan/loan.service';
import { monthNames } from 'src/app/constants/constant';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  userId: string;
  userName: string = "";
  userData: any = {};
  userLoanData: any = [];
  isAccountUser: boolean = true;



  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private loanService: LoanService,
  ) { }

  ngOnInit() {

    this.userId = this.activatedRoute.snapshot.paramMap.get('id');
    this.userService.getUserById(this.userId).subscribe(response => {
      this.userName = `${response.data.firstName} ${response.data.middleName} ${response.data.lastName}`
      this.userData = response.data
      this.isAccountUser = response.data.isAccount ? true : false
    })

    this.fetchUserLoanDetail()
  }

  fetchUserLoanDetail() {
    this.loanService.getLoanByMobileNumber({ mobileNumber: this.userId }).subscribe(response => {
      if(response.data.loanDetails) {
        this.userLoanData = response.data.loanDetails;
      }
    })

  }

  formatMaritalStatus(status: string): string {
    switch (status) {
      case "1":
        return "Single";
      case "2":
        return "Married";
      case "3":
        return "Divorced";
      case "4":
        return "Widowed";
      case "5":
        return "Separated";
      default:
        return status
    }
  }

  formatEmploymentStatus(status: string): string {
    switch (status) {
      case "1":
        return "Full Time Employment";
      case "2":
        return "Part Time Employment";
      case "3":
        return "Pensioner";
      case "4":
        return "Casual";
      case "5":
        return "Unemployed";
      case "6":
        return "Student";
      case "7":
        return "Self-employed";
      default:
        return status
    }
  }

  formatDate(date): string {
    let day = date.slice(6)
    let month = +date.slice(4, 6) - 1;
    let year = date.slice(0, 4)

    return `${day}-${monthNames[month]}-${year}`
  }

}
