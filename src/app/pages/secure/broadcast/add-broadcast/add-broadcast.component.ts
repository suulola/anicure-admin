import { stateAndLocalGovt } from 'src/app/constants/nigeria_state';
import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { genderSelection } from 'src/app/constants/constant';
import { ISelection } from 'src/app/models/iselection';
import { AgencyService } from 'src/app/services/agency_banking/agency.service';
import { UtilsService } from 'src/app/services/utils/utils.service';
import { PermissionService } from 'src/app/services/permission/permission.service';


@Component({
  selector: 'app-add-broadcast',
  templateUrl: './add-broadcast.component.html',
  styleUrls: ['./add-broadcast.component.scss']
})
export class AddBroadcastComponent implements OnInit {

  broadcastMessageForm: FormGroup;
  isLoadingResults = false;
  userNameList: string[] = [];

  constructor(
    public dialogRef: MatDialogRef<AddBroadcastComponent>,
    public formBuilder: FormBuilder,
    public agencyService: AgencyService,
    private utilService: UtilsService,
    private permissionService: PermissionService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }


  ngOnInit() {

      this.broadcastMessageForm = this.formBuilder.group({
        title: new FormControl('', Validators.required),
        message: new FormControl('', Validators.required),
        recipientType: new FormControl('', Validators.required),
        mobileNumber: new FormControl(''),
        selectedUsers: new FormControl(''),
      });
  }

  showSearchByMobile = false;
  searchingUser = false;


  handleRecipientSelection() {
    if(this.broadcastMessageForm.value.recipientType === 'some') {
      this.showSearchByMobile = true;
    } else {
      this.showSearchByMobile = false;
    }

  }

  getUser() {
    this.searchingUser = true
    let model = { mobileNumber: this.broadcastMessageForm.value.mobileNumber }
    this.permissionService.searchUser(model).subscribe(result => {
      this.searchingUser = false
      if (result.status === true && result.data.length > 0) {
        this.userNameList = result.data
      } else {
        this.utilService.triggerNotification("No result found for this search")      
      }
    }, err => {
      this.utilService.triggerNotification("System couldn't fetch users, please try again later")      
    })
  }

  sendMessage() {

  }



  close(): void {
    this.dialogRef.close();
  }

}
