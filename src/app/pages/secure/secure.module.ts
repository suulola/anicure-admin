import { BranchesComponent } from './report/branches/branches.component';
import { LoanActionComponent } from './loan/loan-action/loan-action.component';
import { AddBroadcastComponent } from './broadcast/add-broadcast/add-broadcast.component';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { ReceiptComponent } from './agency/agency-transaction/receipt/receipt.component';
import { AddAgentComponent } from './agency/agency-agents/add-agent/add-agent.component';
import { AssignTerminalComponent } from './agency/agency-agents/assign-terminal/assign-terminal.component';
import { ThirdPartiesComponent } from './report/third-parties/third-parties.component';
import { LogComponent } from './report/log/log.component';
import { UpdateAdminComponent } from './admin/viewusers/update-admin/update-admin.component';
import { AgentPurseComponent } from './agency/agent-purse/agent-purse.component';
import { CentralPurseComponent } from './agency/central-purse/central-purse.component';
import { ViewusersComponent } from './admin/viewusers/viewusers.component';
import { SettingModalComponent } from './agency/agency-settings/setting-modal/setting-modal.component';
import { AgencySettingsComponent } from './agency/agency-settings/agency-settings.component';
import { AuthImagePipe } from './../../utils/auth-image.pipe';
import { LoanSettingsComponent } from './loan/loan-settings/loan-settings.component';
import { LoanManagementComponent } from './loan/loan-management/loan-management.component';
import { ReferralComponent } from './report/referral/referral.component';
import { FeedbackComponent } from './report/feedback/feedback.component';
import { LoanDetailComponent } from './loan/loan-detail/loan-detail.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { UsersComponent } from './users/users.component';
import { AgencyTransactionComponent } from './agency/agency-transaction/agency-transaction.component';
import { AgencyAnalyticsComponent } from './agency/agency-analytics/agency-analytics.component';
import { AuditComponent } from './admin/audit/audit.component';
import { LoanDashboardComponent } from './loan/loan-dashboard/loan-dashboard.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { PermissionsComponent } from './admin/permissions/permissions.component';
import { SecureRoutingModule } from './secure-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { SideMenuComponent } from './../../shared/side-menu/side-menu.component';
import { SidenavService } from '../../services/sidenav/sidenav.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecureComponent } from './secure.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSortModule } from '@angular/material/sort';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { NgChartjsModule } from 'ng-chartjs';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table'  
import { MatPaginatorModule } from '@angular/material/paginator'  
import { MatNativeDateModule } from '@angular/material/core';
import { AgencyAgentsComponent } from './agency/agency-agents/agency-agents.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { ExportAsModule } from 'ngx-export-as';
import { MatSnackBarModule  } from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { BnNgIdleService } from 'bn-ng-idle';
import { AppVersionComponent } from './admin/app-version/app-version.component'
import { AddBranchComponent } from './report/branches/add-branch/add-branch.component'

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatSelectModule,
    MatMenuModule,
    MatSidenavModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule,
    MatDividerModule,
    MatSortModule,
    MatGridListModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    SecureRoutingModule,
    ChartsModule,
    NgChartjsModule,
    ExportAsModule,
  ],
  declarations: [
    SecureComponent,
    DashboardComponent,
    FeedbackComponent,
    ReferralComponent,
    PermissionsComponent,
    LoanDetailComponent,
    TransactionsComponent,
    SideMenuComponent,
    LoanDashboardComponent,
    LoanSettingsComponent,
    LoanManagementComponent,
    AuditComponent,
    ResetpasswordComponent,
    AgencyAnalyticsComponent,
    AgencyAgentsComponent,
    AgencyTransactionComponent,
    UsersComponent,
    UserDetailComponent,
    AgencySettingsComponent,
    SettingModalComponent,
    AddAgentComponent,
    ReceiptComponent,
    AuthImagePipe,
    ViewusersComponent,
    CentralPurseComponent,
    AgentPurseComponent,
    UpdateAdminComponent,
    BroadcastComponent,
    LogComponent,
    BranchesComponent,
    ThirdPartiesComponent,
    AddBroadcastComponent,
    AssignTerminalComponent,
    LoanActionComponent,
    AppVersionComponent,
    AddBranchComponent,
  ],
  providers: [SidenavService, MatDatepickerModule, AuthImagePipe, BnNgIdleService],
  entryComponents: [
    LoanDetailComponent,
    SettingModalComponent,
    UpdateAdminComponent,
    AddAgentComponent,
    ReceiptComponent,
    AddBroadcastComponent,
    LoanActionComponent,
    AssignTerminalComponent,
    AppVersionComponent,
    AddBranchComponent,
  ]
})
export class SecureModule { }
