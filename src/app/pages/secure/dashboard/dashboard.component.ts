import { UtilsService } from 'src/app/services/utils/utils.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { dashboardTopTabs } from 'src/app/constants/constant';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  Object = Object;
  userName = "Anon"

  dateForm: FormGroup;
  dashboardTabs: any = dashboardTopTabs;



  constructor(
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    if (sessionStorage.user) {
      const user = JSON.parse(sessionStorage.user);
      this.userName = `${user.firstName} ${user.lastName}`
    }
    this.dateForm = this.formBuilder.group({
      startDate: new FormControl(new Date(new Date().getFullYear(), 0, 1).toISOString(), Validators.compose([Validators.required])),
      endDate: new FormControl(new Date(new Date().getFullYear(), 11, 31).toISOString(), Validators.compose([Validators.required]))
    });
  }


}
