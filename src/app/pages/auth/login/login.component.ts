import { UtilsService } from './../../../services/utils/utils.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  loading: boolean = false;
  loginForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private utilService: UtilsService,
  ) { }

  login(): void {

    if (!this.loginForm.valid) {
      this.utilService.triggerNotification('Invalid Login Details');
      return;
    }
    this.loading = true;
    console.log(this.loginForm.value)

    sessionStorage.setItem('authorization', "authToken");
    sessionStorage.setItem('user', JSON.stringify(this.loginForm.value));
    this.router.navigate(['/app/dashboard']);
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      mobileNumber: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10)])),
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(4)])),
    });

    if (sessionStorage.getItem("authorization")) {
      this.router.navigate(['/app/dashboard']);
    }

  }

}
