import { AppVersionComponent } from './../../pages/secure/admin/app-version/app-version.component';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { onSideNavChange, animateText } from '../../utils/animations/animations'
import { SidenavService } from 'src/app/services/sidenav/sidenav.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
  animations: [onSideNavChange, animateText]
})
export class SideMenuComponent implements OnInit, AfterViewInit {

  public sideNavState: boolean = false;
  public linkText: boolean = false;
  dynamic: boolean = true;




  pages: any = [
    { name: 'Dashboard', link: '/app/dashboard', icon: 'home' },
    { name: 'Doctors', link: '/app/doctors', icon: 'person' },
    { name: 'Users', link: '/app/users', icon: 'person' },
    // { name: 'Broadcast', permission: 'users', link: '/app/broadcast', icon: 'cast' },
    // { name: 'Card Management', permission: 'users', link: '/app/broadcast', icon: 'payments' },
  ]
  nestedPages: any = [
    {
      title: 'Reports',
      icon: 'account_box',
      list: [
        {
          name: "Payment",
          link: "/app/report/feedback",
          icon: "payments"
        },
        // {
        //   name: "Referral",
        //   permission: 'referral',
        //   link: "/app/report/referral",
        //   icon: "directions"
        // },
        {
          name: "Log",
          permission: 'referral',
          link: "/app/report/log",
          icon: "notes"
        },
        // {
        //   name: "Audit",
        //   permission: 'audit',
        //   link: "/app/setting/audit",
        //   icon: "account_box"
        // },
        // {
        //   name: "Change Password",
        //   permission: 'change_password',
        //   link: "/app/setting/password",
        //   icon: "security"
        // },
        // {
        //   name: "Change App Version",
        //   permission: 'change_password',
        //   method: true,
        //   icon: "app_settings_alt"
        // },
      ]

    },
  ]



  constructor(
    private _sidenavService: SidenavService,
    public dialog: MatDialog,

  ) { }

  async ngAfterViewInit(): Promise<void> {
    
  }

  openAppVersionModal() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '40vw';
    dialogConfig.maxHeight = 'auto';
    const dialogRef = this.dialog.open(AppVersionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(_ => {

    });
  }

  ngOnInit() {
    
  }

  onSinenavToggle(state) {
    if(state === 'menu') {
      this.dynamic = !this.dynamic;
      this.sideNavState = true
    }else {
      if(this.dynamic) {
      this.sideNavState = !this.sideNavState
    }
    }

    setTimeout(() => {
    this.linkText = this.sideNavState;
    }, 200)
    this._sidenavService.sideNavState$.next(this.sideNavState)
  }

}
