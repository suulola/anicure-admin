import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

baseUrl: string = environment.baseUrl;

constructor(private http: HttpClient) {}

// filterUsers(limit: number, page: number, model: any): Observable<any> {
//   return this.http.post<any>(`${this.baseUrl}users/searchUser?limit=${limit}&page=${page}`, model);
// }

getAllUsers(limit: number, page: number): Observable<any> {
  return this.http.get<any>(`${this.baseUrl}users?limit=${limit}&page=${page}`);
}

accountRegistrationType(model): Observable<any> {
  return this.http.post<any>(`${this.baseUrl}users/admin/accountTypes/count`, model);
}

accountReferralChart(model): Observable<any> {
  return this.http.post<any>(`${this.baseUrl}users/admin/account/referral`, model);
}

getCurrentlyLoggedInUsers(): Observable<any> {
  return this.http.get<any>(`${this.baseUrl}users/admin/loggedInUsers`);
}

getUserById(mobileNumber: string): Observable<any> {
  return this.http.get<any>(`${this.baseUrl}users?mobileNumber=${mobileNumber}&full=${true}`);
}

}
