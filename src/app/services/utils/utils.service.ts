import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {

constructor(
  private _snackBar: MatSnackBar,
) { }

triggerNotification(message: string, duration?: number) {

  const snackBarRef = this._snackBar.open(message, 'dismiss', {
    duration: duration || 3000,
    horizontalPosition: 'end',
    verticalPosition: 'bottom'
  });

  snackBarRef.onAction().subscribe(() => {
    snackBarRef.dismiss();
  })

}

formatDateString = (date) => {
  return `${new Date(date).toDateString()} ${new Date(date).toLocaleTimeString()}`;
 }

}
