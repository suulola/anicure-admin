import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl: string = environment.baseUrl;

constructor(private http: HttpClient) { }

login(loginDetail: any): Observable<any> {
  return this.http.post<any>(`${this.baseUrl}users/admin/login`, loginDetail);
}
logOutUser(logoutDetail: any): Observable<any> {
  return this.http.post<any>(`${this.baseUrl}users/admin/logout`, logoutDetail);
}

getAppVersion(): Observable<any> {
  return this.http.get<any>(`${this.baseUrl}app/getVersion`);
}

updateAppVersion(model: any): Observable<any> {
  return this.http.get<any>(`${this.baseUrl}app/updateVersion`, model);
}

}