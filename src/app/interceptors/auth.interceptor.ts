import {
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

export class AuthIntercept implements HttpInterceptor {
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
       if(sessionStorage.getItem('authorization')) {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${sessionStorage.getItem('authorization')}`
            }
        });
       }else {
           console.log('no auth')
       }
        return next.handle(request)
    }
}