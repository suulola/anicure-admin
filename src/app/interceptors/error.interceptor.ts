import {
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

export class ErrorIntercept implements HttpInterceptor {
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                retry(1),
                catchError((error: HttpErrorResponse) => {
                    let errorMessage: any = {};
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage.message = error.error.message;
                    } else {
                        // server-side error
                        errorMessage.status = error.status;
                        errorMessage.message = error.error.message ?? error.message;
                    }
                    return throwError(errorMessage);
                })
        )
    }
}